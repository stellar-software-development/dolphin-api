"use strict";
const config_service_1 = require("./config.service");
const config = new config_service_1.ConfigService(`${process.env.NODE_ENV || 'development'}.env`);
module.exports = config.getTypeOrmConfig();
//# sourceMappingURL=typeorm-cli.config.js.map