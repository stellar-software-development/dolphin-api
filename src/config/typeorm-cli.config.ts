import { ConfigService } from './config.service';

const config = new ConfigService(
  `${process.env.NODE_ENV || 'development'}.env`,
);

export = config.getTypeOrmConfig();
